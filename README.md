# Wiki autosuggestion proxy

Simple proxy between your frontend and wiki autosuggestion writen in GoLang

### You Will Need ###
1. A go environment http://golang.org/doc/install#install


### Installation ###
1. `go get github.com/gorilla/mux` (http://www.gorillatoolkit.org/pkg/mux)
2. `go get github.com/golang/glog` (https://github.com/golang/glog)
3. `cd $GOPATH/src`
4. `git clone https://gitlab.com/pawel.palka/wiki-autosuggestion.git`
5. `cd wiki-autosuggestion`
6. `go run main.go`
7. Navigate to `http://127.0.0.1:8081/api/?q=Pawel`
