package api

import (
	"encoding/json"
	"net/http"
	"time"
	"net/url"
)

const (
	DefaultFetchTimeout = 5 * time.Second
	DefaultUserAgent    = "Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:41.0) Gecko/20100101 Firefox/41.0"
)


type Fetcher struct {
	UserAgent    string
	FetchTimeout time.Duration
}

type FetcherResult [][]interface{}

func (f *Fetcher) Fetch(url *url.URL) (FetcherResult, error) {
	req, err := http.NewRequest("GET", url.String(), nil)
	req.Header.Set("User-Agent", f.UserAgent)
	req.Header.Set("Accept", "application/json, text/javascript, */*; q=0.01")
	req.Header.Set("Accept-Language", "gzip, deflate")
	req.Header.Set("Accept-Encoding", "application/json, text/javascript, */*; q=0.01")

	client := &http.Client{
		Timeout: f.FetchTimeout,
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	fetchResult := FetcherResult{}
	json.NewDecoder(resp.Body).Decode(&fetchResult)
	return fetchResult, nil
}

// New returns an initialized Fetcher.
func NewFetcher() *Fetcher {
	return &Fetcher{
		UserAgent:    DefaultUserAgent,
		FetchTimeout: DefaultFetchTimeout,
	}
}
