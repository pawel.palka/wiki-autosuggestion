package api

import (
	"encoding/json"
	"wiki-autosuggestion/app/common"
	"net/http"
	"net/url"
	"wiki-autosuggestion/app"
)

type ResultElement struct {
	Suggestion  string `json:"suggestion"`
	Description string `json:"description"`
	Url         string `json:"url"`
}

func makeUrlParameters(req *http.Request) *url.URL {
	Url, _ := url.Parse("https://en.wikipedia.org/w/api.php")
	parameters := url.Values{}
	parameters.Add("action", "opensearch")
	parameters.Add("format", "json")
	parameters.Add("formatversion", "2")
	parameters.Add("namespace", "0")
	parameters.Add("limit", "10")
	parameters.Add("suggest", "true")
	parameters.Add("search", req.FormValue("q"))
	Url.RawQuery = parameters.Encode()
	return Url
}

/**
Json API handler for suggestion from wiki
*/
func SuggestionHandler(response http.ResponseWriter, req *http.Request) {
	results, err := NewFetcher().Fetch(makeUrlParameters(req))
	response.Header().Set("Access-Control-Allow-Origin", app.Config.AccessControlAllowOrigin)
	response.Header().Set("Content-Type", "application/json")

	if err != nil {
		common.LogError(err, common.LOG_LEVEL_WARNING)
		http.Error(response, "[]", http.StatusInternalServerError)
		return
	}

	outputResults := make([]ResultElement, len(results[2]))
	for i, _ := range results[2] {
		outputResults[i].Suggestion = results[1][i].(string)
		outputResults[i].Description = results[2][i].(string)
		outputResults[i].Url = results[3][i].(string)
	}
	jsData, err := json.Marshal(outputResults)
	if err != nil {
		common.LogError(err, common.LOG_LEVEL_WARNING)
		http.Error(response, "[]", http.StatusInternalServerError)
		return
	}

	response.Write(jsData)
}
