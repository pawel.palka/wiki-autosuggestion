package app

type ConfigStruct struct {
	ListenAddr   string //addr format used for net.Dial; required
	AccessControlAllowOrigin string
}

var Config = &ConfigStruct{
	ListenAddr:   ":8081",
	AccessControlAllowOrigin:    "*",
}
