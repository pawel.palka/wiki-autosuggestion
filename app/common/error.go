package common

import (
	"log"
	"runtime"

	"github.com/golang/glog"
)

const (
	LOG_LEVEL_INFO    = 0
	LOG_LEVEL_WARNING = 1
	LOG_LEVEL_ERROR   = 2
	LOG_LEVEL_FATAL   = 3
)

func LogError(err error, level int) {
	if err != nil {
		var stack [4096]byte
		runtime.Stack(stack[:], false)
		log.Printf("%q\n%s\n", err, stack[:])

		switch level {
		case LOG_LEVEL_INFO:
			glog.Infoln("%q\n%s\n", err)
		case LOG_LEVEL_WARNING:
			glog.Warningln("%q\n%s\n", err)
		case LOG_LEVEL_ERROR:
			glog.Errorln("%q\n%s\n", err)
		case LOG_LEVEL_FATAL:
			glog.Fatalln("%q\n%s\n", err)
		}

		glog.Flush()
	}
}
