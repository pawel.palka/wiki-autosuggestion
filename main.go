package main

import (
	"flag"
	"wiki-autosuggestion/app"
	"wiki-autosuggestion/app/api"
	"wiki-autosuggestion/app/common"
	"github.com/golang/glog"
	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"net/http"
	"time"
)

func main() {
	flag.Parse()
	defer glog.Flush()

	router := mux.NewRouter()
	http.Handle("/", middleware(router))

	//Json Api handlers
	router.HandleFunc("/api/", api.SuggestionHandler).Methods("GET")

	err := http.ListenAndServe(app.Config.ListenAddr, nil)

	if err != nil {
		common.LogError(err, common.LOG_LEVEL_FATAL)
	}
}

func middleware(router http.Handler) http.Handler {
	return handlers.CompressHandler(
		http.HandlerFunc(func(w http.ResponseWriter, req *http.Request) {
		startTime := time.Now()
		router.ServeHTTP(w, req)

		finishTime := time.Now()
		elapsedTime := finishTime.Sub(startTime)

		switch req.Method {
		case "GET":
			common.LogAccess(w, req, elapsedTime)
		case "POST":
			// here we might use http.StatusCreated
		}

	}))
}
